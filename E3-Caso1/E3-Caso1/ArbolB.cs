﻿using System;
using System.Collections.Generic;
using System.Text;

namespace E3_Caso1
{
    class ArbolB
    {
        public Nodo raiz;
        public Nodo GetRaiz()
        {
            return raiz;
        }
        public void InOrden (Nodo raiz)
        {
            if (raiz != null)
            {
                InOrden(raiz.Izquierdo);
                Console.WriteLine("ID:{0}  Nombre:{1} {2}    Carrera:{3}", raiz.ID, raiz.Nombre, raiz.Apellido, raiz.Carrera);
                InOrden(raiz.Derecho);
            }

        }
        public void  PreOrden(Nodo raiz)
        {
            if (raiz !=null)
            {
                Console.WriteLine("ID:{0}  Nombre:{1} {2}    Carrera:{3}", raiz.ID,raiz.Nombre, raiz.Apellido, raiz.Carrera);
                PreOrden(raiz.Izquierdo);
                PreOrden(raiz.Derecho);
            }
        }
        public void PostOrden(Nodo raiz)
        {
            if (raiz !=null)
            {
                PostOrden(raiz.Izquierdo);
                PostOrden(raiz.Derecho);
                Console.WriteLine("ID:{0}  Nombre:{1} {2}    Carrera:{3}", raiz.ID, raiz.Nombre, raiz.Apellido, raiz.Carrera);

            }
        }
        public void InsertarNodo(int id, string nombre, string apellido, string carrera)
            {
                Nodo puntero;
                Nodo padre;
                Nodo nodo=new Nodo
                {
                    ID = id, Nombre = nombre, Apellido = apellido, Carrera =carrera
                };
                if (raiz !=null)
                {
                puntero = raiz;
                while (true)
                {
                    padre = puntero;
                    if (id < puntero.ID)
                    {
                        puntero = puntero.Izquierdo;
                        if (puntero ==null)
                        {
                            padre.Izquierdo = nodo;
                            break;
                        }
                    }
                    else
                    {
                        puntero = puntero.Derecho;
                        if(puntero ==null)
                        {
                            padre.Derecho = nodo;
                            break;
                        }
                    }
                }
             }
             else
             {
                raiz = nodo;
             }

        }
        
    }
}
