﻿using System;

namespace E3_Caso1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("...............Ingreso de postulantes...............\n");
            ArbolB arbol = new ArbolB();
            arbol.InsertarNodo(1085,"Hugo", "Huatuco", "Ing. de sitemas Computacionales");
            arbol.InsertarNodo(1545,"Jose", "Linares", "Derecho");
            arbol.InsertarNodo(1044,"Arturo", "Castillo", "Administración");
            arbol.InsertarNodo(1078,"Pedro", "Suarez", "Medicina Humana");
            arbol.InsertarNodo(1099,"Maria", "Palacios", "Contabilidad y Finanzas");

            Console.WriteLine("...............Preorden...............");
            arbol.PreOrden(arbol.GetRaiz());
            Console.WriteLine("");

            Console.WriteLine("...............InOrder...............");
            arbol.InOrden(arbol.GetRaiz());
            Console.WriteLine("");
            
            Console.WriteLine("...............Postorden...............");
            arbol.PostOrden(arbol.GetRaiz());
            Console.WriteLine("");
            Console.ReadLine();
        }
    }
}
