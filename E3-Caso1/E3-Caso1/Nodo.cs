﻿using System;
using System.Collections.Generic;
using System.Text;

namespace E3_Caso1
{
    class Nodo
    {
        public int ID;
        public string Nombre;
        public string Apellido;
        public string Carrera;
        public Nodo Izquierdo;
        public Nodo Derecho;
    }
}
