﻿using System;
using System.Collections.Generic;
using System.Text;

namespace E3_Caso2
{
    class Nodo
    {
        public int ID;
        public string HC;
        public string Paciente;
        public Nodo Izquierdo;
        public Nodo Derecho;
    }
}
