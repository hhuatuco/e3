﻿using System;
using System.Collections.Generic;
using System.Text;

namespace E3_Caso2
{
    class ArbolB
    {
        public Nodo raiz;
        public Nodo GetRaiz()
        {
            return raiz;
        }
        public void InOrden(Nodo raiz)
        {
            if (raiz != null)
            {
                InOrden(raiz.Izquierdo);
                Console.WriteLine("= {0} {1} {2}", raiz.ID, raiz.HC,raiz.Paciente);
                InOrden(raiz.Derecho);
            }
        }
        public void InsertarNodo(int id , string hc, string paciente)
        {
            Nodo puntero;
            Nodo padre;
            Nodo nodo = new Nodo
            {
                ID = id, HC = hc, Paciente = paciente
            };

            if (raiz != null)
            {
                puntero = raiz;
                while (true)
                {
                    padre = puntero;
                    if (id < puntero.ID)
                    {
                        puntero = puntero.Izquierdo;
                        if (puntero == null)
                        {
                            padre.Izquierdo = nodo;
                            break;
                        }
                    }
                    else
                    {
                        puntero = puntero.Derecho;
                        if (puntero == null)
                        {
                            padre.Derecho = nodo;
                            break;
                        }
                    }
                }
            }
            else
            {
                raiz = nodo;
            }
        }

        public void BuscarPorLlave(int llave)
        {
            int contador = 0;
            Nodo puntero = raiz;
            while (puntero != null)
            {
                contador +=1;
                if (puntero.ID == llave)
                {
                    Console.WriteLine("Historia clínica: {0} encontrada", puntero.ID);
                    Console.WriteLine("Paciente: {0} ", puntero.Paciente);
                    Console.WriteLine("Total de iteraciones realizadas:" + contador);
                    return;
                }
                else
                {
                    if (llave > puntero.ID)
                    {
                        puntero = puntero.Derecho;
                    }
                    else
                    {
                        puntero = puntero.Izquierdo;
                    }
                }
            }
        }


    }
}
